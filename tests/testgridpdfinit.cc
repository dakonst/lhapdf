#include "LHAPDF/GridPDF.h"
#ifdef HAVE_MPI
#include <mpi.h>
#endif
int main(int argc, char* argv[]) {
#ifdef HAVE_MPI
  MPI_Init(&argc, &argv);
#endif

  LHAPDF::GridPDF("tests/testpdf/testpdf_0000.dat");
  return 0;
#ifdef HAVE_MPI
  MPI_Finalize();
#endif  
}
